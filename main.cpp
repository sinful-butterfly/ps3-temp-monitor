#include <cstdio>
#include <iostream>
#include <vector>
#include "ccapi.h"

static CCAPI ccapi;

using namespace std;
int tempLimit = 65;

int checkTemps(int cell, int rsx) {
	if (cell > tempLimit || rsx > tempLimit) {
		return -1;
	}
	else {
		return 0;
	}
}


int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;


	if (!ccapi.GetLibraryState())
	{
		cout << "CCAPI Library couldn't be loaded." << endl
			<< "Check that you are using the correct CCAPI.dll." << endl;
	}
	else
	{
		vector<CCAPI::ConsoleInfo> cInfo = ccapi.GetConsoleList();

		for (u32 c = 0; c < cInfo.size(); ++c)
		{
			cout << "Console: " << cInfo.at(c).name << " Ip:" << cInfo.at(c).ip << endl;
		}

		if (ccapi.Connect("192.168.43.118") == CCAPI_OK)
		{
			cout << "Connected to your PS3." << endl;
			ccapi.VshNotify(CCAPI::NotifyTrophy1, "Connected to your console.");
			ccapi.RingBuzzer(CCAPI::BuzzerDouble);

			u32 firmware, version;
			CCAPI::ConsoleType cType;
			int cell, rsx;
			int ret = ccapi.GetFirmware(&firmware) |
				ccapi.GetConsoleType(&cType) |
				ccapi.GetVersion(&version) |
				ccapi.GetTemperature(&cell, &rsx);
			for (;;) {
				int ret = checkTemps(cell, rsx);
				cout << "CELL: " + to_string(cell) + "\n";
				cout << "RSX: " + to_string(rsx) + "\n";
				if (ret == -1) {
					cout << "Your console is running hot.\n";
					ccapi.VshNotify(CCAPI::NotifyCaution, "Your console is running hot.");
					ccapi.RingBuzzer(CCAPI::BuzzerDouble);
				}
				else {
					cout << "Temps are ok";
				}
				Sleep(10000);
				system("CLS");
			}

			
		}
		else
		{
			
			cout << "Couldn't connect to your console, make sure you typed the IP properly.\n";
			exit(-1);
			
		}
	}
	ccapi.Disconnect();

	return 0;
}
